## GOLEM
```
npm i
node ./src/server.js
```

****

La rama development (desarrollo) de este proyecto se despliega automáticamente en https://golem-api.dev.walmartretail.cloud. // sólo test con los usuarios de prueba expresados más abajo, en este ambiente la aplicación no se conecta con Auth0 sino que responde con un mockup local.
La rama master (producción) de este proyecto se despliega automáticamente en https://golem-api.prod.walmartretail.cloud. 

Para pruebas locales debe usar http://localhost:3000.

Haga una petición tipo POST con los headers: 'Content-Type': 'application/json' junto con un objeto en el body:
```
{
    "username": "USERNAME", // parámetro requerido
    "password": "PASSWORD", // parámetro requerido
    "payload": { "unaclave": "unvalor" }, // parámetro requerido
    "tokenDuration": 7654,”1s”, “1m”,”1h” // parámetro opcional / default 3h  }
}
```

Los usuarios de prueba son:
```
[ { "user_id": 1, "username": "user1", "password": "pass1" }, { "user_id": 2, "username": "user2", "password": "pass2" }, { "user_id": 3, "username": "user3", "password": "pass3" }, "user_id": 4, "username": "failauth0", "password": "failauth0" } ]

/* El usuario failauth0 retorna el error que ocurre cuando la comunicación con auth0 falla  */
```

Si el resultado es correcto obtendrá un json con el token => 
```
{
    "golem_token": "<un_token>",
    "golem_key_pub": "-----BEGIN PUBLIC KEY-----\nMFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAJhvRjDFiN7DFAk7AVnyD9KsU/U13oy0\n/M9nw0rx2+eOv8SG8vE9yUiz2sCSJCDIjTWzIf3YXZlQF1joTBLusr8CAwEAAQ==\n-----END PUBLIC KEY-----\n"
}
```

Si el resultado es incorrecto obtendrá los siguientes errores => 
```
[{ 
    "error": true, "message": "invalid_credentials" // username/password inválido
},
{ 
    "error": true, "message": "internal_error" // Falla de comunicación con auth0
}]
```

****

3) Haga una petición tipo POST a http://localhost:3000/api/verifytoken con los headers: 'Content-Type': 'application/json' junto con un objeto en el body: 
```
{
	"token": < token recibido a través de la petición a /api/login >
}
```
```
```
****

Cuando se hace una petición al endpoint /api/login se instruye el guardado de los datos de la petición en una tabla de mongodb con los siguientes campos:
```
{
    url: { type: String, trim: true },
    registerDate: { type: Date, default: Date.now },
    method: { type: String, trim: true },
    data: { type: Schema.Types.Mixed },
    headers: { type: Schema.Types.Mixed }
};
```

Se plantea authenticacion por api key con llaves pùblicas y privadas