/*jshint esversion: 6 */
/* jshint node: true */

const Constant = require('../../config.json');
const auth0Helpers = require('../utils/auth0Helpers');

exports.authenticateUserFake = function (username, password, callback) {
  console.log('authenticateUserFake');
  let fails = 0;
  Constant.USERS_FAKE.map((val) => {
    if (username === 'failauth0') {
      callback("internal_error", false);
      return;
    } else if ((val.username !== username || val.password !== password) && fails++ === Constant.USERS_FAKE.length - 1) {
      callback('invalid_credentials', false);
      return;
    } else if ((val.username === username && val.password === password)) {
      callback(undefined, true);
      return;
    }
  });
};

exports.authenticateUser = function (username, password, type, callback) {
  auth0Helpers.authenticateUser(username, password, type, (err, isValid) => {
    if (err) {
      callback(err, false);
    } else {
      callback(undefined, isValid);
    }
  });
};

exports.updatePasswordProccess = function (username, password, metaData, callback) {
  auth0Helpers.validateMetaData(metaData, (err, isValid) => {
    if (err) {
      callback(err);
    } else {
      auth0Helpers.getAuth0User(username, (err, userData) => {
        if (err) {
          callback(err);
        } else {
          if (userData.length < 1) {
            callback(errorHandling.handleError('G001'));
          } else {
            const auth0Id = userData[0].user_id;

            auth0Helpers.updatePassword(auth0Id, password, false, (err, updateResponse) => {
              if (err) {
                callback(err);
              } else {
                callback(undefined, updateResponse);
              }
            });
          }
        }
      });
    }
  })
};
