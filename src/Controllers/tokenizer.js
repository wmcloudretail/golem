/*jshint esversion: 6 */
/* jshint node: true */
const jwt = require("jsonwebtoken");
const Constant = require("../../config.json");

// PRIVATE and PUBLIC key
const privateKey = Constant.PRIVATE_KEY;
const publicKey = Constant.PUBLIC_KEY;

function generateToken(payload, duration) {
  try {
    const signOptions = {
      algorithm: "RS256",
      expiresIn: duration || "3h",
      issuer: Constant.ISSUER,
      audience: Constant.AUDIENCE
      // subject: username
    };

    const token = jwt.sign(payload, privateKey, signOptions);
    console.info('El token fue generado exitosamente');
    return token;
  }
  catch (err) {
    console.error('El token no pudo ser generado', err);
    throw "token_generation_internal_error";
  }
}

function verifyToken(token) {
  try {
    jwt.verify(token, publicKey);
    console.info('El token fue verificado exitosamente');
    return true;
  }
  catch (err) {
    console.error(err);
    throw err.message;
  }
}

module.exports = { generateToken, verifyToken };
