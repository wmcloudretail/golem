const auth0Helpers = require('../utils/auth0Helpers');
const ServicesADP = require('../Services/golem-adp-db');
const errorHandling = require('../utils/errorHandling');

exports.getSecurityQuestions = function (username, callback) {
  ServicesADP.adpUserServices.findUser({ rut: username }, {}, {}, (err, res) => {
    if (err) {
      callback('internal_error', false);
    } else {
      if (res) {
        try {
          const { streetNumber } = res;
          const number = streetNumber.match(/(\d+)(?!.*\d)/g)[0];
          const streetNumberForValidation = streetNumber.split(number).join('XXXX');
          const questions = [
            {
              question: "¿Cuál es el nombre de tu AFP?",
              attributeToCheck: "afp"
            },
            {
              question: "Indica tu fecha de nacimiento",
              attributeToCheck: "birthdate"
            },
            {
              question: "¿Cuál es tu fecha de ingreso a la compañia?",
              attributeToCheck: "dateOld"
            },
            {
              question: "Última comuna de residencia ingresada en Walmart",
              attributeToCheck: "commune"
            },
            {
              question: `Indique la numeración faltante: tú dirección es ${streetNumberForValidation}`,
              attributeToCheck: "streetNumber"
            },
          ];
          callback(undefined, questions);
        } catch (error) {
          callback('internal_error', false);
        }
      } else {
        callback('user_not_found', false);
      }
    }
  })
};

exports.validateSecurityQuestions = function (metaData, callback) {
  auth0Helpers.validateMetaData(metaData, (err, isValid) => {
    if (err) {
      callback(err);
    } else {
      callback(undefined, isValid);
    }
  });
};

exports.validateRutAgainstCivilRegister = function (metaData, callback) {
  try {
    ServicesADP.adpUserServices.findUser({ rut: metaData.run }, {}, {}, async (err, user) => {
      if (err) {
        callback('internal_error', false);
      } else {
        if (user) {
          try {
            const data = user.nationality !== 'Chilena' ? Object.assign({}, metaData, { documentType: 'CEDULA_EXT' }) : metaData;
            const isValid = await auth0Helpers.validateUserAgainstCivilRegistry(data);
            callback(undefined, isValid);
          } catch (error) {
            callback(error);
          }
        } else {
          callback(errorHandling.handleError('G001'));
        }
      }
    })
  } catch (error) {
    callback(errorHandling.handleError('G000'));
  }
};

exports.getApplicationSecurity = name => {
  return new Promise((resolve, reject) => {
    ServicesADP.adpUserServices.findSecurity({ applicationName: name }, { _id: 0 }, {}, (err, res) => {
      if (err) {
        reject(new Error('internal error finding applicationName: ' + name))
      } else {
        if (res) {
          const { applicationName, privateKey, routes } = res;
          resolve({ applicationName, privateKey, routes })
        } else {
          reject(new Error('application not found: ' + name))
        }
      }
    });
  });
};

exports.createAdpExternalUser = function (userData, callback) {
  try {
    ServicesADP.adpExternalUserServices.findExternalUser({ _id: userData.rut }, {}, {}, (err, user) => {
      if (err) {
        callback('internal_error', false);
      } else {
        if (user) {
          callback('user_already_exist', false);
        } else {
          const { rut } = userData;
          auth0Helpers.getAuth0User(rut, (err, auth0User) => {
            if (err) {
              callback(err);
            } else {
              if (auth0User && auth0User[0] && auth0User[0].user_id) {
                callback('user_already_exist', false);
              } else {
                ServicesADP.adpExternalUserServices.createExternalUser(userData, (err, res) => {
                  if (err) {
                    callback('internal_error', false);
                  } else {
                    const type = 'EXTERNAL';
                    auth0Helpers.createAuth0User(userData, type, (err, response) => {
                      if (err) {
                        callback(err);
                      } else {
                        callback(undefined, 'USUARIO CREADO EXITOSAMENTE');
                      }
                    });
                  }
                })
              }
            }
          });
        }
      }
    })
  } catch (error) {
    callback(errorHandling.handleError('G000'));
  }
};

exports.deleteAdpExternalUser = function (username, callback) {
  try {
    ServicesADP.adpExternalUserServices.findExternalUser({ _id: username }, {}, {}, (err, user) => {
      if (err) {
        callback('internal_error', false);
      } else {
        if (user) {
          ServicesADP.adpExternalUserServices.deleteExternalUser({ _id: username }, (err, res) => {
            if (err) {
              callback('internal_error', false);
            } else {
              auth0Helpers.deleteAuth0User(username, (err, res) => {
                if (err) {
                  callback(err);
                } else {
                  callback(undefined, res);
                }
              })
            }
          })
        } else {
          callback('user_does_not_exist', false);
        }
      }
    })
  } catch (error) {
    callback(errorHandling.handleError('G000'));
  }
};
