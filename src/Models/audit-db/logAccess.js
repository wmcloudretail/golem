'use strict';

const conn = require('../../DbConnections');
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const logAccess = new Schema({
  url: { type: String, trim: true },
  registrerDate: { type: Date, default: Date.now },
  method: { type: String, trim: true },
  data: { type: Schema.Types.Mixed },
  headers: { type: Schema.Types.Mixed }
});

module.exports = conn.dbAudit.model('logaccess', logAccess);