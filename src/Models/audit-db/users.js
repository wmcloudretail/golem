/*jshint esversion: 6 */
/* jshint -W097 */
/* jshint node: true */
'use strict';

const conn = require('../../DbConnections');
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const users = new Schema({
  email: { type: String, trim: true, index: true, unique: true },
  email_verified: { type: Boolean, default: true },
  username: { type: String, trim: true, index: true, unique: true },
  user_metadata: { first_login: { type: Boolean } }
});

module.exports = conn.dbAudit.model('user', users);
