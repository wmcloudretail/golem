'use strict';

const mongoose = require('mongoose');
const conn = require('../../DbConnections');

const Schema = mongoose.Schema;

const security = new Schema(
  {
    applicationName: String,
    privateKey: String,
    routes: [String]
  }
);

module.exports = conn.dbAdp.model('Security', security, 'serviceSecurity');
