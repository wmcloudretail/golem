'use strict';

const mongoose = require('mongoose');
const conn = require('../../DbConnections');

const Schema = mongoose.Schema;

const children = new Schema(
  {
    from: Date,
    until: Date,
    member: String,
    birthdate: Date,
    name: String,
    fatherLastName: String,
    motherLastName: String,
    rut: String,
    age: String,
    sexo: String,
  }
);

module.exports = conn.dbAdp.model('Children', children);