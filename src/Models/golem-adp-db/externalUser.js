'use strict';

const mongoose = require('mongoose');
const conn = require('../../DbConnections');

const Schema = mongoose.Schema;

const externalUser = new Schema(
  {
    _id: String,
    rut: String,
    name: String,
    secondName: String,
    fatherLastName: String,
    motherLastName: String,
    emailOffice: String,
    mobile: String,
    function: String,
    position: String,
    nameBoss: String,
    rutBoss: String,
    birthdate: Date,
    nationality: String,
    isExternal: { type: Boolean, default: true },
    isEnabled: { type: Boolean, default: true },
    localNumber: {
      type: String,
      default: '0000'
    },
    regionLocal: String,
    communeLocal: String
  }
)

module.exports = conn.dbAdp.model('ExternalUser', externalUser);