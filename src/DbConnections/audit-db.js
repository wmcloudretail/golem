const mongoose = require("mongoose");
const Constant = require('../../config.json');

const options = { useNewUrlParser: true, useCreateIndex: true };

const auditDBConn = mongoose.createConnection(Constant.dbURI, options);

module.exports = auditDBConn;
