const mongoose = require("mongoose");
const Constant = require('../../config.json');

const options = { useNewUrlParser: true, useCreateIndex: true };

const adpDb = mongoose.createConnection(Constant.dbURIADP, options);

module.exports = adpDb;
