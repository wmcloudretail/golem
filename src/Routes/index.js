/*jshint esversion: 6 */
/* jshint node: true */
const Constant = require('./../../config.json');
const routes = require('express').Router();
const Services = require('../Services/audit-db');
const Controllers = require('../Controllers');
const Auth0Helpers = require('../utils/auth0Helpers');
const ServicesADP = require('../Services/golem-adp-db');
const _ = require('lodash');
const auth = require("../Middleware/auth");

class ResponseError {
  constructor(message) {
    this.message = message;
    this.error = true;
  }
}

routes.post(Constant.endPoint.login, auth, (req, res) => {
  if (!req.body.username || !req.body.password || !req.body.payload)
    return res.json(new ResponseError('Los campos username, password y payload son requeridos'));

  const params = {
    username: req.body.username, password: req.body.password,
    payload: req.body.payload, tokenDuration: req.body.tokenDuration
  };

  saveReqToDB(req, res, _.omit(params, 'password'));
  ServicesADP.adpExternalUserServices.findExternalUser({ rut: params.username }, { _id: 1 }, (err, externalUser) => {
    if (err) {
      return res.json(new ResponseError('internal_error'));
    }
    if (externalUser) {
      Auth0Helpers.getAuth0User(params.username, (error, user) => {
        if (user && user.length >= 1) {
          let firstLogin = user[0].user_metadata.first_login;
          if (firstLogin) {
            Auth0Helpers.updatePassword(user[0].user_id, `Walmart${params.username.substring(0, 5)}`, true, () => { });
            return res.json(new ResponseError('unactivated_user'));
          }

          // Try authenticate external user
          Controllers.auth0.authenticateUser(params.username, params.password, 'EXTERNAL', (error, isValid) => {
            // If valid generate token and return response
            if (isValid) {
              res.json(createToken(params.payload, params.tokenDuration));
            }
            else
              res.json(new ResponseError(error));
          });
        } else {
          return res.json(new ResponseError('user_not_found'));
        }
      });
    } else {
      // Find user in mongo
      ServicesADP.adpUserServices.findUser({ rut: params.username }, { _id: 1 }, {}, (err, adpUser) => {
        if (err) {
          return res.json(new ResponseError('internal_error'));
        }

        if (adpUser) {
          Auth0Helpers.getAuth0User(params.username, (error, user) => {
            if (user && user.length >= 1) {
              let firstLogin = user[0].user_metadata.first_login;
              if (firstLogin) {
                Auth0Helpers.updatePassword(user[0].user_id, `Walmart${params.username.substring(0, 5)}`, true, () => { });
                return res.json(new ResponseError('unactivated_user'));
              }

              // Try authenticate walmart user
              Controllers.auth0.authenticateUser(params.username, params.password, 'INTERN', (error, isValid) => {
                // If valid generate token and return response
                if (isValid) {
                  res.json(createToken(params.payload, params.tokenDuration));
                }
                else
                  res.json(new ResponseError(error));
              });
            } else {
              return res.json(new ResponseError('user_not_found'));
            }
          });
        } else {
          return res.json(new ResponseError('user_not_found'));
        }
      });
    }
  });
});

function createToken(payload, tokenDuration) {
  try {
    const token = Controllers.tokenizer.generateToken(payload, tokenDuration);
    return { golem_token: token, golem_key_pub: Constant.PUBLIC_KEY };
  }
  catch (err) {
    return new ResponseError(err);
  }
}

routes.post(Constant.endPoint.verifytoken, auth, (req, res) => {
  if (!req.body.token)
    res.json(new ResponseError('El campo token es requerido'));
  else {
    try {
      const isValid = Controllers.tokenizer.verifyToken(req.body.token);
      res.json({ tokenVerified: isValid });
    }
    catch (err) {
      res.json(new ResponseError(err));
    }
  }
});

routes.post(Constant.endPoint.updatepassword, auth, (req, res) => {
  const data = req.body.metaData;
  if (!data || !data.username || !data.password) {
    res.json(new ResponseError('Los campos username, password y metadata son requeridos'));
  } else {
    try {
      saveReqToDB(req, res, _.omit(data, 'password'));
      Controllers.auth0.updatePasswordProccess(data.username, data.password, data, (err, response) => {
        if (err) {
          res.json(new ResponseError(err));
        } else {
          res.json(createToken({ rut: response.username }));
        }
      })
    } catch (err) {
      res.json(new ResponseError(err));
    }
  }
});

routes.post(Constant.endPoint.activateuser, auth, async (req, res) => {
  const data = req.body.metaData;
  if (!data || !data.username || !data.password) {
    res.json(new ResponseError('Los campos username, password y metadata son requeridos'));
  } else {
    try {
      saveReqToDB(req, res, _.omit(data, 'password'));
      Controllers.auth0.updatePasswordProccess(data.username, data.password, data, (err, response) => {
        if (err) {
          res.json(new ResponseError(err));
        } else {
          res.json(createToken({ rut: response.username }));
        }
      })
    } catch (err) {
      res.json(new ResponseError(err));
    }
  }
});

routes.get(Constant.endPoint.securityquestions, auth, (req, res) => {
  const { username } = req.query;
  saveReqToDB(req, res, {});
  Controllers.adp.getSecurityQuestions(username, (err, response) => {
    if (err) {
      res.json(new ResponseError(err));
    } else {
      res.json(response);
    }
  })
});

routes.post(Constant.endPoint.validatesecurityquestions, auth, (req, res) => {
  const { metaData } = req.body;
  if (!metaData || !metaData.username) {
    return res.json(new ResponseError("El campo metaData es requerido"));
  } else {
    saveReqToDB(req, res, {});
    Controllers.adp.validateSecurityQuestions(metaData, (err, response) => {
      if (err) {
        res.json(new ResponseError(err));
      } else {
        res.json(response);
      }
    });
  }
});

routes.get(Constant.endPoint.getuserdata, auth, (req, res) => {
  saveReqToDB(req, res, {});
  const { username } = req.query;
  const projection = {
    name: 1,
    secondName: 1,
    fatherLastName: 1,
    motherLastName: 1,
    emailOffice: 1,
    mobile: 1,
    function: 1,
    localNumber: 1,
    position: 1,
    nameBoss: 1,
    regionLocal: 1,
    communeLocal: 1
  };

  try {
    ServicesADP.adpExternalUserServices.findExternalUser({ rut: username }, projection, {}, (err, adpExternalUser) => {
      if (err) {
        res.json(new ResponseError(err));
      } else {
        if (adpExternalUser) {
          res.json(adpExternalUser);
        } else {
          ServicesADP.adpUserServices.findUser({ rut: username }, projection, {}, (err, adpUser) => {
            if (err) {
              res.json(new ResponseError(err));
            } else {
              if (adpUser) {
                res.json(adpUser);
              } else {
                res.json(new ResponseError('user_not_found'));
              }
            }
          })
        }
      }
    });
  }
  catch (err) {
    res.json(new ResponseError(err));
  }
});

routes.post(Constant.endPoint.validaterutagainstcivilregister, auth, (req, res) => {
  const data = req.body.metaData;
  if (!data || !data.run || !data.documentType || !data.documentNumber) {
    res.json(new ResponseError('Los campos run, documentType, documentNumber y metadata son requeridos'));
  } else {
    try {
      saveReqToDB(req, res, _.omit(data, 'documentNumber'));
      Controllers.adp.validateRutAgainstCivilRegister(data, (err, response) => {
        if (err) {
          res.json(new ResponseError(err));
        } else {
          res.json(response);
        }
      });
    } catch (error) {
      res.json(new ResponseError(err));
    }
  }
});

routes.put(Constant.endPoint.createadpexternaluser, auth, (req, res) => {
  const data = req.body;
  let { rut, name, secondName, fatherLastName, motherLastName, emailOffice, mobile, position, nameBoss, rutBoss, birthdate, nationality, regionLocal, communeLocal, localNumber } = data;
  if (!rut || !name || !secondName || !fatherLastName || !motherLastName || !emailOffice || !mobile || !data.function || !position || !nameBoss || !rutBoss || !birthdate || !nationality || !regionLocal || !communeLocal) {
    res.json(new ResponseError('Los campos rut, name, secondName, fatherLastName, motherLastName, emailOffice, mobile, position, nameBoss, rutBoss, birthdate, nationality, regionLocal y communeLocal son requeridos'));
  } else {
    try {
      saveReqToDB(req, res, data);
      data.function = data.function.toUpperCase();
      position = position.toUpperCase();
      rut = rut.toUpperCase();
      const userToCreate = {
        rut,
        name,
        secondName,
        fatherLastName,
        motherLastName,
        emailOffice,
        mobile,
        position,
        nameBoss,
        rutBoss,
        birthdate,
        nationality,
        regionLocal,
        communeLocal,
      };
      if (localNumber) {
        userToCreate.localNumber = localNumber;
      }
      Controllers.adp.createAdpExternalUser(userToCreate, (err, response) => {
        if (err) {
          res.json(new ResponseError(err));
        } else {
          res.json(response);
        }
      });
    } catch (error) {
      res.json(new ResponseError(error));
    }
  }
});

routes.delete(Constant.endPoint.deleteadpexternaluser, auth, (req, res) => {
  const data = req.body;
  if (!data.username) {
    res.json(new ResponseError('El campo username es requerido'));
  } else {
    try {
      saveReqToDB(req, res, data);
      data.username = data.username.toUpperCase();
      Controllers.adp.deleteAdpExternalUser(data.username, (err, response) => {
        if (err) {
          res.json(new ResponseError(err));
        } else {
          res.json(response);
        }
      });
    } catch (error) {
      res.json(new ResponseError(error));
    }
  }
});

const saveReqToDB = function (req, res, data) {
  const dataRequest = { url: req.url, method: req.method, data: data, headers: req.headers };
  Services.logAccessServices.createLog(dataRequest, (err, res) => {
    if (err) {
      throw new Error(err);
    }
  });
};

module.exports = routes;
