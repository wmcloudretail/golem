'use strict'

exports.handleError = (err) => {
  let message;
  switch (err) {
    case 'G000':
      message = 'Hubo un error al intentar procesar tu solicitud.';
      break;
    case 'G001':
      message = 'Usuario no encontrado.';
      break;
    case 'G002':
      message = 'Existen problemas con la contraseña utilizada.';
      break;
    case 'G003':
      message = 'Es necesario responder al menos 3 preguntas.';
      break;
    case 'G004':
      message = 'El número de respuestas correctas es menor a 3.';
      break;
    case 'G005':
      message = 'Formato de metadata inválido.';
      break;
    case 'G006':
      message = 'Hubo un error al intentar validar contra el Registro Civil';
      break;
    case 'G007':
      message = 'El documento indicado no está vigente';
      break;
    case 'G008':
      message = 'ApplicationName not found in the registry';
      break;
    case 'G009':
      message = 'Not Authorized to access this route';
      break;
    case 'G010':
      message = 'Invalid Token Format';
      break;
    case 'G011':
      message = 'No token or applicationName provided';
      break;
    default:
      message = 'Hubo un error al intentar procesar tu solicitud.';
      break;
  }
  return message;
};
