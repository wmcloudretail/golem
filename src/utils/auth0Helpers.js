'use strict'

const requestCookies = require('request-with-cookies').createClient();
const request = require('request');
const moment = require('moment');
const parser = require('node-html-parser').parse;
const Constant = require('../../config.json');
const errorHandling = require('../utils/errorHandling');
const ServicesADP = require('../Services/golem-adp-db');

exports.getAppCredentials = getAppCredentials;
exports.validateUserAgainstCivilRegistry = __validateUserAgainstCivilRegistry;
exports.getAuth0User = getAuth0User;

function getAppCredentials(callback) {
  const getAppCredentials = {
    url: `https://${Constant.AUTH0_CUSTOM_DOMAIN}/oauth/token`,
    headers: { 'content-Type': 'application/json' },
    form: {
      audience: Constant.AUTH0_APP_AUDIENCE,
      client_id: Constant.AUTH0_APP_CLIENT_ID,
      grant_type: Constant.AUTH0_APP_CLIENT_GRANT_TYPE,
      client_secret: Constant.AUTH0_APP_CLIENT_SECRET,
    }
  }
  request.post(getAppCredentials, function (error, response, body) {
    try {
      body = JSON.parse(body);
      if (error || response.statusCode != 200) {
        callback(errorHandling.handleError('G000'));
      } else {
        const credentials = {
          access_token: body.access_token,
          scope: body.scope,
          expires_in: body.expires_in,
          token_type: body.token_type,
        }
        callback(undefined, credentials);
      }
    } catch (err) {
      callback(errorHandling.handleError('G000'));
    }
  });
}

function getAuth0User(username, callback) {
  getAppCredentials((error, credentials) => {
    if (error) callback(error);
    const userRequest = {
      url: `https://${Constant.AUTH0_CUSTOM_DOMAIN}/api/v2/users?q=username:"${username}"&search_engine=v3`,
      json: true,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${credentials.access_token}`,
      },
    };

    request.get(userRequest, function (error, response, body) {
      if (error) {
        callback(errorHandling.handleError('G000'));
      } else {
        if (response.statusCode !== 200) {
          callback(errorHandling.handleError('G000'));
        } else {
          callback(undefined, body);
        }
      }
    });
  });
}

exports.validateMetaData = async function (metaData, callback) {
  const { questions = {}, username } = metaData;
  const questionkeys = Object.keys(questions) || [];
  let isValid = false;
  try {
    if (questions && questionkeys.length > 0) {
      isValid = await __validateUserQuestions(questions, username);
      callback(undefined, isValid);
    } else {
      ServicesADP.adpUserServices.findUser({ rut: username }, {}, {}, async (err, user) => {
        if (err) {
          callback(errorHandling.handleError('G001'));
        } else {
          try {
            if (!user) {
              callback(errorHandling.handleError('G001'));
            } else {
              const data = user.nationality !== 'Chilena' ? Object.assign({}, metaData, { documentType: 'CEDULA_EXT' }) : metaData;
              // Validate the document model
              if (!isValidMetadata(data)) {
                callback(errorHandling.handleError('G005'));
              } else {
                isValid = await __validateUserAgainstCivilRegistry(data);
                callback(undefined, isValid);
              }
            }
          } catch (error) {
            callback(error);
          }
        }
      })
    }
  } catch (err) {
    callback(err);
  }
};

exports.updatePassword = function (auth0id, passwordToUpdate, firstLogin, callback) {
  getAppCredentials((error, credentials) => {
    if (error) callback(error);

    const updatePassRequest = {
      url: `https://${Constant.AUTH0_CUSTOM_DOMAIN}/api/v2/users/${auth0id}`,
      json: true,
      body: {
        password: passwordToUpdate,
        connection: Constant.AUTH0_CUSTOM_REALM,
        user_metadata: {
          first_login: firstLogin
        },
      },
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${credentials.access_token}`,
      },
    };
    request.patch(updatePassRequest, (error, response, body) => {
      if (error) {
        callback(errorHandling.handleError('G000'));
      } else if (response.statusCode !== 200) {
        if (body && body.error) {
          callback(errorHandling.handleError('G002'));
        }
        else {
          callback(errorHandling.handleError('G000'));
        }
      }
      else {
        callback(undefined, body)
      }
    });
  });
};

exports.authenticateUser = function (username, password, type, callback) {
  const userEmailType = type == 'EXTERNAL' ? '@wmexternal.cl' : '@mio.cl';
  const authenticateUserParams = {
    url: `https://${Constant.AUTH0_CUSTOM_DOMAIN}/oauth/token`,
    headers: { 'content-type': 'application/json' },
    form: {
      grant_type: Constant.AUTH0_CUSTOM_GRANT_TYPE,
      audience: Constant.AUTH0_CUSTOM_AUDIENCE,
      scope: Constant.AUTH0_CUSTOM_SCOPE,
      client_id: Constant.AUTH0_CUSTOM_CLIENT_ID,
      realm: Constant.AUTH0_CUSTOM_REALM,
      username: `${username}${userEmailType}`,
      password: password
    }
  };

  request.post(authenticateUserParams, function (error, response, body) {
    try {
      if (error) {
        callback('internal_error', false);
      } else {
        body = JSON.parse(body);
        if (body.error == 'invalid_grant') {
          if (body.error_description === 'user is blocked') {
            callback('user_blocked', false);
          } else {
            callback('invalid_credentials', false);
          }
        } else if (body.error === 'too_many_attempts') {
          callback('too_many_attempts', false);
        } else if (body.error === 'access_denied') {
          callback('access_denied', false);
        } else {
          callback(undefined, true);
        }
      }
    } catch (error) {
      callback('internal_error', false);
    }
  });
};

exports.createAuth0User = function (userData, type, callback) {
  getAppCredentials((error, credentials) => {
    if (error) callback(error);
    const mailString = type == 'EXTERNAL' ? 'wmexternal.cl' : 'mio.cl';
    const createUserRequest = {
      url: `https://${Constant.AUTH0_CUSTOM_DOMAIN}/api/v2/users`,
      json: true,
      body: {
        connection: Constant.AUTH0_CUSTOM_REALM,
        name: `${userData.rut}@${mailString}`,
        username: userData.rut,
        email: `${userData.rut}@${mailString}`,
        user_metadata: {
          first_login: type == 'EXTERNAL' ? false : true
        },
        email_verified: false,
        verify_email: false,
        app_metadata: {},
        password: `Walmart${userData.rut.substring(0, 5)}`
      },
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${credentials.access_token}`,
      },
    };
    
    request.post(createUserRequest, function (error, response, body) {
      try {
        if (error) {
          callback('internal_error', false);
        } else {
          if (body.statusCode != 200) {
            callback('internal_error', false);
          } else {
            callback(undefined, body);
          }
        }
      } catch (error) {
        callback('internal_error', false);
      }
    });
  });
};

exports.deleteAuth0User = function (username, callback) {
  getAppCredentials((error, credentials) => {
    if (error) callback(error);
    getAuth0User(username, (err, auth0User) => {
      if (err) {
        callback(err);
      } else {
        if (auth0User && auth0User[0] && auth0User[0].user_id) {
          const auth0id = auth0User[0].user_id;
          const deleteUserRequest = {
            url: `https://${Constant.AUTH0_CUSTOM_DOMAIN}/api/v2/users/${auth0id}`,
            json: true,
            body: {
              connection: Constant.AUTH0_CUSTOM_REALM
            },
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${credentials.access_token}`,
            },
          };
          request.delete(deleteUserRequest, function (error, response, body) {
            try {
              if (error) {
                callback('internal_error', false);
              } else {
                if (response.statusCode != 204) {
                  callback('auth0_error', false);
                } else {
                  callback(undefined, body);
                }

              }
            } catch (error) {
              callback('internal_error', false);
            }
          });
        } else {
          callback('user_does_not_exist', false);
        }
      }
    });
  });
};

function __validateUserQuestions(questions, username) {
  return new Promise(async (resolve, reject) => {

    const answered = Object.keys(questions).reduce((prev, curr) => {
      if (Constant.USER_QUESTIONS_MODEL.includes(curr)) {
        return Object.assign({}, prev, {
          [curr]: questions[curr],
        });
      }
      return prev;
    }, {});

    if (Object.keys(answered).length < 3) {
      reject(errorHandling.handleError('G003'));
    }

    ServicesADP.adpUserServices.findUser({ rut: username }, { afp: 1, dateOld: 1, birthdate: 1, commune: 1, streetNumber: 1 }, {}, (err, user) => {
      if (err) {
        callback(errorHandling.handleError('G001'));
      } else {
        if (user) {
          const correctAnswers = Object.keys(answered).reduce((prev, curr, idx) => {
            if (curr.indexOf('date') !== -1) {
              const userDate = moment(user[curr]).utc().format('DD-MM-YYYY');
              const answerDate = answered[curr];

              return userDate === answerDate ? prev + 1 : prev;
            }

            if (curr === 'streetNumber') {
              if (user.streetNumber) {
                const number = user.streetNumber.match(/(\d+)(?!.*\d)/g)[0];
                return answered[curr] === number ? prev + 1 : prev;
              } else {
                return prev;
              }
            }

            if (user[curr] === answered[curr].toUpperCase()) {
              return prev + 1;
            }
            return prev;
          }, 0);

          if (correctAnswers < 3) {
            reject(errorHandling.handleError('G004'));
          }
          resolve(true);
        } else {
          reject(errorHandling.handleError('G001'));
        }
      }
    })
  })
}

async function __validateUserAgainstCivilRegistry(metaData) {
  return new Promise((resolve, reject) => {

    const model = metaData;

    // First GET the HTML page and extract the java session :P parameter
    const javaSessionRequest = {
      rejectUnauthorized: false,
      url: Constant.CIVIL_REGISTRY_ENDPOINT,
    }

    requestCookies.get(javaSessionRequest, (error, response, body) => {
      if (error) {
        reject(errorHandling.handleError('G006'));
      }
      try {
        const $ = parser(response.body);
        // Get the cookie and java session ^^ to passs through to the next POST call
        const javaSession = $.querySelectorAll('input').find(
          input => input.id === 'javax.faces.ViewState'
        ).attributes.value;
        // cleanse run for clean formatting
        let cleanRun = model.run.replace(/\./g, '').replace(/\-/g, '');
        cleanRun = `${cleanRun.substring(
          0,
          cleanRun.length - 1
        )}-${cleanRun.substring(cleanRun.length - 1)}`;
        // VALIDATE DOCUMENT NUMBER WITH THE JAVA SESSION CREATED

        const validationRequest = {
          rejectUnauthorized: false,
          followAllRedirects: true,
          url: Constant.CIVIL_REGISTRY_ENDPOINT,
          form: {
            form: 'form',
            'form:run': cleanRun,
            'form:selectDocType': model.documentType || 'CEDULA',
            'form:docNumber': model.documentNumber,
            'javax.faces.ViewState': javaSession,
            'form:buttonHidden': '',
          },
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
        }
        requestCookies.post(validationRequest, (error, response, body) => {
          if (error) {
            reject(errorHandling.handleError('G006'));
          }
          try {
            const $ = parser(response.body);
            const state = $.querySelector('#section2 td.setWidthOfSecondColumn').text;
            switch (state) {
              case 'Vigente':
                return resolve(true);
              case 'No Vigente':
                return reject(errorHandling.handleError('G007'));
              default:
                return reject(errorHandling.handleError('G006'));
            }
          } catch (error) {
            reject(errorHandling.handleError('G006'));
            return;
          }
        });
      } catch (error) {
        reject(errorHandling.handleError('G006'));
      }
    });
  })
}

function isValidMetadata(metaData) {
  let isValid = false;
  if ("run" in metaData && metaData.run != null) {
    if ("documentType" in metaData && metaData.documentType != null) {
      if (Constant.DOCUMENT_TYPES.includes(metaData.documentType)) {
        if ("documentNumber" in metaData && metaData.documentNumber != null) {
          isValid = true;
        }
      }
    }
  }
  return isValid;
}