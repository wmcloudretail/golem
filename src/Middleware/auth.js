const uuidAPIKey = require('uuid-apikey');
const Controllers = require('../Controllers');
const MessageErr = require('../utils/errorHandling');

module.exports = async function (req, res, next) {
  const requestPath = req._parsedUrl.pathname;
  const applicationName = req.headers["applicationname"];
  const token = req.headers["x-access-token"] || req.headers["authorization"];
  if (!token || !applicationName) return res.status(401).send(MessageErr.handleError('G011'));

  try {

    const result = await Controllers.adp.getApplicationSecurity(applicationName);
    if (!result) {
      res.status(404).send(MessageErr.handleError('G008'));
    }

    if (uuidAPIKey.check(token, result.privateKey) && result.routes.includes(requestPath)) {
      next();
    } else {
      res.status(401).send(MessageErr.handleError('G009'));
    }

  } catch (ex) {
    res.status(500).send(MessageErr.handleError('G010'));
  }
};
