/*jshint esversion: 6 */
'use strict';

const Models = require('../../Models/audit-db')

const createuser = function (objToSave, callback) {
    new Models.users(objToSave).save(callback)
};

const findUser = function (criteria, proyection, options, callback) {
    options.lean = true;
    Models.users.findOne(criteria, proyection, options, callback);
};

const findUsers = function (criteria, proyection, options, callback) {
    options.lean = true;
    Models.users.find(criteria, proyection, options, callback);
};

const updateUser = function (criteria, objToSave, options, callback) {
    options.lean = true;
    options.new = false;
    options.returnNewDocument = true;
    Models.users.updateOne(criteria, objToSave, options, callback);
};

module.exports = {
    createuser,
    updateUser,
    findUser,
    findUsers
};