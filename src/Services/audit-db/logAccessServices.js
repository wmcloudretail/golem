/*jshint esversion: 6 */
/* jshint node: true */

'use strict';

const Models = require('../../Models/audit-db');

const createLog = function (objToSave, callback) {
    new Models.logAccess(objToSave).save( callback);
};

module.exports = {
    createLog
};