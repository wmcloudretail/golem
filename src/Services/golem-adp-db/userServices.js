'use strict';

const Models = require('../../Models/golem-adp-db');

const createUser = function (objToSave, callback) {
  new Models.user(objToSave).save(callback);
};

const findUser = function (criteria, proyection, options, callback) {
  options.lean = true;
  Models.user.findOne(criteria, proyection, options, callback);
};

const updateUser = function (criteria, proyection, options, callback) {
  options.lean = true;
  options.new = false;
  options.returnNewDocument = true;
  Models.user.updateOne(criteria, proyection, options, callback);
};

const findSecurity = function (criteria, proyection, options, callback) {
  options.lean = true;
  Models.security.findOne(criteria, proyection, options, callback);
};

module.exports = {
  createUser,
  updateUser,
  findUser,
  findSecurity
};
