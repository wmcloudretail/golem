'use strict';

const Models = require('../../Models/golem-adp-db');

const createExternalUser = function (objToSave, callback) {
  objToSave._id = objToSave.rut;
  new Models.externalUser(objToSave).save(callback);
};

const findExternalUser = function (criteria, proyection, options, callback) {
  options.lean = true;
  Models.externalUser.findOne(criteria, proyection, options, callback);
};

const updateExternalUser = function (criteria, proyection, options, callback) {
  options.lean = true;
  options.new = false;
  options.returnNewDocument = true;
  Models.externalUser.updateOne(criteria, proyection, options, callback);
};

const deleteExternalUser = function (criteria, callback) {
  Models.externalUser.findOneAndRemove(criteria, { useFindAndModify: false }, callback);
};

module.exports = {
  createExternalUser,
  findExternalUser,
  updateExternalUser,
  deleteExternalUser
};