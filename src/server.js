/*jshint esversion: 6 */
const express = require('express');
const morgan = require('morgan');
const app = express();
const bodyParser = require('body-parser');
const routes = require('./Routes');
const Constant = require('./../config.json');
const conn = require('./DbConnections');
const uuidAPIKey = require('uuid-apikey');
const Controllers = require('./Controllers');

const PORT = Constant.PORT || 3000;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  // authorized headers for preflight requests
  // https://developer.mozilla.org/en-US/docs/Glossary/preflight_request
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();

  app.options('*', (req, res) => {
    // allowed XHR methods
    res.header('Access-Control-Allow-Methods', 'GET, POST');
    res.send();
  });
});

/* Conectar todas las rutas de la app */
app.use('/', routes);

app.use(function(err, req, res, next) {
  // logic
  console.log(err);
  next();
});

/* Realizar la conexión a todas las BD */
waitForConnections();

/* Función para la conexión a todas las BD */
function waitForConnections() {
  Promise.all([conn.dbAudit, conn.dbAdp])
    .then((values) => {
      console.info('All DB Connected');
      serverStart();
    })
    .catch((err) => {
      console.log('All DB Not Connected', err)
      process.exit(1);
    });
}

/* Logica de inicio de servidor, en este caso solo se levanta el servidor en el puerto 3000 */
function serverStart() {
  app.listen(PORT, function () {
    console.info(`El endpoint corre en localhost:${Constant.PORT}${Constant.endPoint.login}`);
  });
}
